package com.geoape.backgroundlocationexample;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class BackgroundService extends Service implements LocationController.LocationListener {
    private final LocationServiceBinder binder = new LocationServiceBinder();
    private final String TAG = "BackgroundService";


    private final int LOCATION_INTERVAL = 500;
    private final int LOCATION_DISTANCE = 10;
    private Handler h = new Handler();
    private int delay = 15 * 1000; //1 second=1000 miliseconds
    private Runnable runnable;
    private double latitide;
    private double longitude;
    private LocationController locationManager;

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        locationManager = new LocationController(this,this);
        locationManager.fetchLocation();
        h.postDelayed(runnable = new Runnable() {
            public void run() {
                //do something
                Log.d(TAG, "timeDelay==" + latitide + "==" + longitude);
                Toast.makeText(BackgroundService.this, "timeDelay=" + latitide + "==" + longitude, Toast.LENGTH_LONG).show();
                h.postDelayed(runnable, delay);
            }
        }, delay);
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        Log.i(TAG, "onCreate");
        //startForeground(12345678, getNotification());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            try {
                locationManager.stopLocationUpdates();
            } catch (Exception ex) {
                Log.i(TAG, "fail to remove location listners, ignore", ex);
            }
        }
    }


    public void stopTracking() {
        this.onDestroy();
    }

    private Notification getNotification() {

        NotificationChannel channel = new NotificationChannel("channel_01", "My Channel", NotificationManager.IMPORTANCE_DEFAULT);

        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);

        Notification.Builder builder = new Notification.Builder(getApplicationContext(), "channel_01").setAutoCancel(true);
        return builder.build();
    }

    @Override
    public void onLocationAvailable(Location location) {
        this.latitide = location.getLatitude();
        this.longitude = location.getLongitude();
        Log.d("onLocationAvailable", "success=" + latitide + "==longitude" + longitude);
        Toast.makeText(this, "location_avai=" + latitide + "==" + longitude, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFail(LocationController.LocationListener.Status status) {
        Log.d("onFail", "not_available_location=");
    }


    public class LocationServiceBinder extends Binder {
        public BackgroundService getService() {
            return BackgroundService.this;
        }
    }

}
