package com.geoape.backgroundlocationexample;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import static com.geoape.backgroundlocationexample.LocationController.LocationListener.Status.ERROR_GPS_LOCATION;
import static com.geoape.backgroundlocationexample.LocationController.LocationListener.Status.NO_PLAY_SERVICE;
import static com.geoape.backgroundlocationexample.LocationController.LocationListener.Status.PERMISSION_DENIED;

public class LocationController implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private Context context;
    private static final int UPDATE_INTERVAL = 7000; // 7 sec
    private static final int FATEST_INTERVAL = 5000; // 5 sec
    private static final int DISPLACEMENT = 5; // 5 meters

    private GoogleApiClient mGoogleApiClient;
    private LocationListener locationListener;
    private LocationRequest mLocationRequest;
    private LocationUpdateCallback mLocationUpdateCallback;
    //Request
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    public final static int FINE_LOCATION_PERMISSION_REQUEST_CODE = 115;

    public LocationController(Context context, LocationListener locationListener) {
        this.context = context;
        this.locationListener = locationListener;
        init();
    }

    public void fetchLocation(){
        String[] LOCATION_PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION};
        if(hasAllPermissions(LOCATION_PERMISSIONS))
         checkLocationEnable();
        else {
             if(context instanceof Activity){
                 Activity activity=(Activity)context;
                 requestLocationPermission(activity);
             }else {
                 Toast.makeText(context, "Location Permission not granted", Toast.LENGTH_LONG).show();
                 locationListener.onFail(PERMISSION_DENIED);
             }
        }

    }

    public  boolean hasAllPermissions(String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    public static boolean requestLocationPermission(Activity context)
    {
        boolean hasPermission = (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(context,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    FINE_LOCATION_PERMISSION_REQUEST_CODE);
            return false;
        }else return true;
    }

    public void init() {
        if (checkPlayServices()) {
            buildGoogleApiClient();
            if (mGoogleApiClient != null) {
                mGoogleApiClient.connect();
            }
        } else {
            if (locationListener != null)
                locationListener.onFail(NO_PLAY_SERVICE);
        }
    }

    public synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApiIfAvailable(LocationServices.API).build();
    }

    private boolean checkPlayServices() {

        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(context);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                if (context instanceof Activity) {
                    Activity activity = (Activity) context;
                    googleAPI.getErrorDialog(activity, result,
                            PLAY_SERVICES_RESOLUTION_REQUEST).show();
                } else {
                    Toast.makeText(context, "Google Play Service Not Available", Toast.LENGTH_LONG).show();
                }

            }
            return false;
        }
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        createLocationRequest();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    @SuppressLint("RestrictedApi")
    protected void createLocationRequest() {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT); // 10 meters

    }

    public void checkLocationEnable() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();
        SettingsClient settingsClient = LocationServices.getSettingsClient(context);
        settingsClient.checkLocationSettings(locationSettingsRequest);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            locationListener.onFail(PERMISSION_DENIED);
            return;
        }
        mLocationUpdateCallback=new LocationUpdateCallback();
        FusedLocationProviderClient mFusedLocationProviderClient=LocationServices.getFusedLocationProviderClient(context);
        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,mLocationUpdateCallback,Looper.myLooper());
        mFusedLocationProviderClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // GPS location can be null if GPS is switched off
                        if (location != null) {
                            locationListener.onLocationAvailable(location);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("MapDemoActivity", "Error trying to get last GPS location");
                        locationListener.onFail(ERROR_GPS_LOCATION);
                    }
                });

    }

    class LocationUpdateCallback extends LocationCallback{
        @Override
        public void onLocationResult(LocationResult locationResult) {
            locationListener.onLocationAvailable(locationResult.getLastLocation());

        }
    }


    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
        {
            FusedLocationProviderClient mFusedLocationProviderClient=LocationServices.getFusedLocationProviderClient(context);
            mFusedLocationProviderClient.removeLocationUpdates(mLocationUpdateCallback);
        }
    }




    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    public interface LocationListener {
        void onLocationAvailable(Location location);

        void onFail(Status status);

        enum Status {
            PERMISSION_DENIED, NO_PLAY_SERVICE, DENIED_LOCATION_SETTING,ERROR_GPS_LOCATION
        }
    }
}
