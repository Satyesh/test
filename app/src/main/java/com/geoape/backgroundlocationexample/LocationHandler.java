package com.geoape.backgroundlocationexample;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

public class LocationHandler extends Service implements LocationManager.LocationListener {
    private final LocationServiceBinder binder = new LocationServiceBinder();
    private LocationManager locationManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        super.onStartCommand(intent, flags, startId);
        locationManager=new LocationManager(this);
        locationManager.triggerLocation(this);
        return START_STICKY;
    }

    @Override
    public void onLocationAvailable(double latitide, double longitude) {
        Log.d("onLocationAvailable","success="+latitide+"==longitude"+longitude);
    }

    @Override
    public void onFail(Status status) {
        Log.d("onFail","not_available_location=");
    }


    public class LocationServiceBinder extends Binder {
        public LocationHandler getService() {
            return LocationHandler.this;
        }
    }
}
